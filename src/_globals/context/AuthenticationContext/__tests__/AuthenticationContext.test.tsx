import React, { createContext, useContext } from "react";
import { mount, shallow } from "enzyme";
import { renderHook, act } from "@testing-library/react-hooks";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router";

import AuthenticationProvider, { AuthenticationContext } from "..";

it("Tests Authentication State :: Login", () => {
    const wrapper = shallow(<AuthenticationProvider />);
    const { result, rerender } = renderHook(() => wrapper.props().value);
    act(() => {
        result.current.Handle_SetIsAuthenticated("login");
    });
    rerender();

    expect(sessionStorage.getItem("isAuthenticated")).toBe("true");
    expect(result.current.isAuthenticated).toBe(true);
});

it("Tests Authentication State :: Logout", () => {
    const wrapper = shallow(<AuthenticationProvider />);
    const { result, rerender } = renderHook(() => wrapper.props().value);

    act(() => {
        result.current.Handle_SetIsAuthenticated("logout");
    });
    rerender();

    expect(sessionStorage.getItem("isAuthenticated")).toBe("false");
    expect(result.current.isAuthenticated).toBe(false);
});

/* it("Tests Authentication State :: useEffect", () => {
    let useEffect;
    const mockUseEffect = () => {
        useEffect.mockImplementationOnce((f: any) => f());
    };
    const wrapper = shallow(<AuthenticationProvider />);
    const { result, rerender } = renderHook(() => wrapper);
    // const myMockFn = jest.spyOn(React, "useEffect").mockImplementationOnce(cb => cb());
    // console.log(myMockFn((err, val) => console.log(val)) );
    useEffect = jest.spyOn(React, "useEffect");
    mockUseEffect();
    console.log(useEffect); //?

    act(async () => {
        result.current.render();
        // result.current.Handle_SetIsAuthenticated("logout");
        // result.current.props().value.Handle_SetIsAuthenticated("login");

        console.log(useEffect.mockReturnValue()());
        sessionStorage.setItem("isAuthenticated", "true");
        // console.log(jest.spyOn(sessionStorage.__proto__, 'getItem').mockReturnValueOnce("isAuthenticated")())
    });
    rerender();
    console.log(result.current); //?
    // expect(sessionStorage.getItem("isAuthenticated")).toBe("false");
    expect(result.current.props().value.isAuthenticated).toBe(true);
});
 */
