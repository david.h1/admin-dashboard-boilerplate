import React, { createContext, ReactElement, useEffect, useState } from "react";

export interface Authenticated {
    isAuthenticated: boolean;
}

export interface Application_AuthContext extends Authenticated {
    Handle_SetIsAuthenticated: (action: string) => void;
}

export const AuthenticationContext = createContext({} as Application_AuthContext);

export default function AuthenticationProvider({ children }: any): ReactElement {
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    useEffect((): any => {
        if (window.sessionStorage.getItem("isAuthenticated") === "true") {
            setIsAuthenticated(true);
            return "true";
        } else {
            return "false";
        }
    }, [isAuthenticated]);

    const Handle_SetIsAuthenticated = (action: string): void => {
        if (action === "login") {
            setIsAuthenticated(true);
            sessionStorage.setItem("isAuthenticated", "true");
        } else if (action === "logout") {
            setIsAuthenticated(false);
            sessionStorage.setItem("isAuthenticated", "false");
        }
    };

    return (
        <AuthenticationContext.Provider value={{ isAuthenticated, Handle_SetIsAuthenticated }}>
            {children}
        </AuthenticationContext.Provider>
    );
}
