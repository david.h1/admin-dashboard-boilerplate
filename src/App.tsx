import React, { ReactElement, useContext } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import ChakraProviderContainer from "./components/_theme";
import PageSwitcher from "./pages/";
import AuthenticationProvider, { AuthenticationContext } from "./_globals/context/AuthenticationContext";
import { Authenticated, Application_AuthContext } from "./_globals/context/AuthenticationContext/index";

/*
 * Route Switcher Container (Pass Props of isAuthenticated for testing purposes)
 */
export function ApplicationContainer({ isAuthenticated }: Authenticated): ReactElement {
    return (
        <Router>
            <PageSwitcher isAuthenticated={isAuthenticated} />
        </Router>
    );
}

/*
 * Required to split up context from the provider a further step to allow for router testing
 ? Consumes AuthenticationContext
 */
export function AuthenticationConsumer(): ReactElement {
    const { isAuthenticated } = useContext<Application_AuthContext>(AuthenticationContext);

    return <ApplicationContainer isAuthenticated={isAuthenticated} />;
}

export default function App(): ReactElement {
    return (
        <ChakraProviderContainer>
            <AuthenticationProvider>
                <AuthenticationConsumer />
            </AuthenticationProvider>
        </ChakraProviderContainer>
    );
}
