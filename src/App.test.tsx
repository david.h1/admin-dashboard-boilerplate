import React from "react";
import { render, screen } from "@testing-library/react";
import { shallow, mount } from "enzyme";
import { MemoryRouter } from "react-router";
import App, { AuthenticationConsumer, ApplicationContainer } from "./App";
import AuthenticationProvider, { AuthenticationContext } from "./_globals/context/AuthenticationContext";

it("renders <App /> without crashing", () => {
    shallow(<App />);
});

it("renders <AuthenticationConsumer /> without crashing", () => {
    shallow(<AuthenticationConsumer />);
});

it("mounts <AuthenticationConsumer /> with context auth [true]", () => {
    const wrapper = mount(
        <MemoryRouter>
            <AuthenticationConsumer />
        </MemoryRouter>,
        {
            wrappingComponent: AuthenticationContext.Provider,
            wrappingComponentProps: {
                value: { isAuthenticated: true },
            },
        }
    );
    expect(wrapper.find(ApplicationContainer).props().isAuthenticated).toBe(true);
});
it("mounts <AuthenticationConsumer /> with context auth [false]", () => {
    const wrapper = mount(
        <MemoryRouter>
            <AuthenticationConsumer />
        </MemoryRouter>,
        {
            wrappingComponent: AuthenticationContext.Provider,
            wrappingComponentProps: {
                value: { isAuthenticated: false },
            },
        }
    );
    expect(wrapper.find(ApplicationContainer).props().isAuthenticated).toBe(false);
});
