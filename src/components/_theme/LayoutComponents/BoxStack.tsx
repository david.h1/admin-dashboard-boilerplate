import { chakra, Stack, useStyleConfig, Box, shouldForwardProp } from "@chakra-ui/react";

interface Props {
    variant?: undefined | keyof typeof CustomVariants;
    size?: any;
    children?: React.ReactNode;
}

export const CustomVariants = {
    LiquidityAndStakingContainer: {
        flexDirection: ["column-reverse", "column-reverse", "row", "row"],
        width: { base: "100%", xl: "1200px", lg: "960px" },
        margin: "auto",
        columnGap: "40px",
        py: { base: 25 },
        px: { base: 4 },
    },
    staking: ({ colorMode }: any) => ({
        flexDirection: "column",
        flex: "7",
        width: "max-content",
    }),
    liquidityMining: ({ colorMode }: any) => ({
        flexDirection: "column",
        flex: "7",
        w: "100%",
    }),
};

export const Variants = {
    baseStyle: ({ colorMode }: any) => ({
        color: colorMode === "light" ? "lightTheme.dark.main" : "darkTheme.light.main",
    }),
    ...CustomVariants,
};

/**
 * Custom variant of Chakra Stack
 *
 * @see {@link Props}
 * @see {@link CustomVariants}
 * @see {@link https://chakra-ui.com/docs/layout/stack}
 * @param variant - Variant options for different layout styles
 * @returns ChakraUI Stack modified with variants
 *
 * @beta
 */
export default function Box_Stack(props: Props) {
    const { size, variant, ...rest } = props;
    const styles = useStyleConfig("Box_Stack", { size, variant });
    return <Stack sx={styles} {...rest} />;
}
