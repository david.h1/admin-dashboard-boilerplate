import Box_Stack, * as BoxStack from "./BoxStack";
import Card_Box, { cardBox } from "./CardBox";
import Header_Box, * as HeaderBox from "./HeaderBox";

import { homepageFlex, Homepage_Flex } from "../_atomic/Templates/";

export { Box_Stack, Card_Box, Header_Box, Homepage_Flex };

const LayoutComponents = {
    Box_Stack: {
        variants: {
            ...BoxStack.Variants,
        },
    },
    Card_Box: {
        ...cardBox,
    },
    Header_Box: {
        variants: {
            ...HeaderBox.Variants,
        },
    },
    // TEMPLATE & CONTAINERS
    Homepage_Flex: {
        ...homepageFlex,
    },
};

export default LayoutComponents;
