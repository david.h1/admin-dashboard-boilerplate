import {
    chakra,
    Stack,
    useStyleConfig,
    Box,
    keyframes,
    usePrefersReducedMotion,
} from "@chakra-ui/react";
import { animation } from "polished";
import { ReactElement } from "react";
import BoxBackgroundPattern from "../../../assets/BoxBackgroundHeader.svg";

interface Props {
    /**
     * {@inheritDoc Book.writeFile}
     * @typeParam T
     */
    variant?: undefined | keyof typeof Variants;
    size?: string;
    children?: React.ReactNode;
    backgroundImage?: any;
    backgroundPosition?: any;
    animation?: any;
}

const CommonStyles = ({ colorMode }: any) => {
    return {
        color: colorMode === "light" ? "lightTheme.dark.main" : "darkTheme.light.main",
        boxShadow: "inset 0px 0px 15px 1px rgba(0,0,0,0.08)",
        bg:
            colorMode === "light"
                ? "lightTheme.background.header.main"
                : "darkTheme.background.header.main",
        // bgImage: BoxBackgroundPattern,
        bgRepeat: "repeat",
        bgSize: "contain",
        backgroundPosition: "50% 50%",
        transition: "background-position 1s linear repeat",
        w: "100%",
        minHeight: "200px",
        maxHeight: "100%",
        display: "flex",
        justifyContent: "center",
    };
};

// Always keep ...CommonStyles({colorMode}) at bottom to detect overrides
export const Variants = {
    baseStyle: ({ colorMode }: any) => ({
        ...CommonStyles({ colorMode }),
    }),
    defaultProps: {
        size: "full",
        variant: "baseStyle",
    },
};

/* export const Variants = {
    ...CustomVariants,
};
 */
/**
 * Returns the average of two numbers.
 *
 * @see {@link CustomVariants}
 * @remarks Variants
 * liquidityMining - Variant
 * @param card - Variant
 * @returns The arithmetic mean of `x` and `y`
 *
 * @beta
 */
export default function Header_Box(props: Props): ReactElement {
    const {
        variant = "baseStyle",
        size = "full",
        backgroundImage,
        backgroundPosition,
        animation,
        ...rest
    } = props;
    const styles = useStyleConfig("Header_Box", { size, variant });
    return (
        <Box
            sx={{ ...styles, backgroundImage, animation /* backgroundPosition */ }}
            {...rest}
        />
    );
}
