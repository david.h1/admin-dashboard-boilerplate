import { chakra, Stack, useStyleConfig, Box } from "@chakra-ui/react";
import GroovePaperBG from "../../../assets/groovepaper.png";

interface Props {
    /**
     * {@inheritDoc Book.writeFile}
     * @typeParam T
     */
    variant?: undefined | keyof typeof Variants;
    size?: string;
    children?: React.ReactNode;
}

// Always keep ...CommonStyles({colorMode}) at bottom to detect overrides
export const Variants = {
    liquidityMining: ({ colorMode }: any) => ({
        p: { base: 5 },
        borderRadius: { base: 10 },
    }),
    textureBackground: ({ colorMode }: any) => ({
        /*         bg:
            colorMode === "light"
                ? "lightTheme.background.header.main"
                : "darkTheme.background.header.main", */
        background: colorMode === "light" ? "#F9F9F9" : "darkTheme.background.darkest",
        bgImage: GroovePaperBG,
        minHeight: "100vh",
        backgroundBlendMode: colorMode === "light" ? "lighten" : "darken",
        backgroundRepeat: "repeat",
        bgSize: "contain",
        zIndex: -2,
    }),
};

export const backgroundBox = {
    baseStyle: ({ colorMode }: any) => ({
        color: colorMode === "light" ? "lightTheme.dark.main" : "darkTheme.light.main",
        bg:
            colorMode === "light"
                ? "lightTheme.background.card.main"
                : "darkTheme.background.card.main",
        boxShadow: "0px 0px 15px 1px rgba(0,0,0,0.12)",
    }),
    variants: {
        ...Variants,
    },
    sizes: {},
    defaultProps: {
        size: "",
        variant: "",
    },
};

/* export const Variants = {
    ...CustomVariants,
};
 */
/**
 * Returns the average of two numbers.
 *
 * @remarks
 * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
 * @see {@link CustomVariants}
 * @remarks Variants
 * liquidityMining - Variant
 * @param card - Variant
 * @returns The arithmetic mean of `x` and `y`
 *
 * @beta
 */
export default function Background_Box(props: Props) {
    const { variant, size, ...rest } = props;
    const styles = useStyleConfig("Card_Box", { size, variant });
    return <Box sx={styles} {...rest} />;
}

/* 
import React, { ReactElement } from "react";
import { Flex, Spacer, useColorModeValue, useColorMode } from "@chakra-ui/react";
import styled from "@emotion/styled";

import GroovePaperBG from "../../assets/groovepaper.png";
import { colors } from "../../theme";

const BackgroundTexture = styled.div<any>`
    background: url(${GroovePaperBG}),
        ${(props) =>
            (props.colorMode == "light" && "#F9F9F9") || colors.background.darkest};
    background-blend-mode: ${(props) =>
        (props.colorMode == "light" && "lighten") || "darken"};
    background-repeat: repeat;
    min-height: 100vh;
    z-index: -2;
`;

interface Props {
    children: any;
}

export default function Background({ children }: Props): ReactElement {
    const { colorMode } = useColorMode();
    return <BackgroundTexture colorMode={colorMode}>{children}</BackgroundTexture>;
}
 */
