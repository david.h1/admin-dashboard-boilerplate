import { chakra, Stack, useStyleConfig, Box } from "@chakra-ui/react";
import BoxBackgroundPattern from "../../assets/BoxBackgroundHeader.svg";

interface Props {
    /**
     * {@inheritDoc Book.writeFile}
     * @typeParam T
     */
    variant?: undefined | keyof typeof Variants;
    size?: string;
    children?: React.ReactNode;
}

// Always keep ...CommonStyles({colorMode}) at bottom to detect overrides
export const Variants = {
    liquidityMining: ({ colorMode }: any) => ({
        p: { base: 5 },
        borderRadius: { base: 10 },
    }),
    card: ({ colorMode }: any) => ({
        w: "100%",
        minHeight: "300px",
        px: "20px",
        py: "30px",
        borderRadius: "20px",
        mx: { base: 5 },
        my: { base: 5 },
    }),
    staking: ({ colorMode }: any) => ({
        p: { base: 5 },
        borderRadius: { base: 10 },
        minHeight: "300px",
    }),
};

export const cardBox = {
    baseStyle: ({ colorMode }: any) => ({
        color: colorMode === "light" ? "lightTheme.dark.main" : "darkTheme.light.main",
        bg:
            colorMode === "light"
                ? "lightTheme.background.card.main"
                : "darkTheme.background.card.main",
        boxShadow: "0px 0px 15px 1px rgba(0,0,0,0.12)",
    }),
    variants: {
        ...Variants,
    },
    sizes: {},
    defaultProps: {
        size: "",
        variant: "",
    },
};

/* export const Variants = {
    ...CustomVariants,
};
 */
/**
 *
 * @remarks
 * @see {@link CustomVariants}
 * @remarks Variants
 * @returns Custom Variant of Card_Box
 *
 * @beta
 */
export default function Card_Box(props: Props) {
    const { variant, size, ...rest } = props;
    const styles = useStyleConfig("Card_Box", { size, variant });
    return <Box sx={styles} {...rest} />;
}
