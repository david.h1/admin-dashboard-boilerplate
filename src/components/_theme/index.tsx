import { ChakraProvider, extendTheme } from "@chakra-ui/react";

// Import and Export LayoutComponent functions - Might be a better way?
import { Box_Stack, Card_Box, Header_Box, Homepage_Flex } from "./LayoutComponents";

import LayoutComponents from "./LayoutComponents";
import GeneralComponents from "./GeneralComponents/GeneralComponents";
import ColorTheme from "./ColorTheme/ColorTheme";

export { Box_Stack, Card_Box, Header_Box, Homepage_Flex };

// Global Styles
const styles = {
    global: (props: any) => ({
        // removes outline unless using [tab] key
        ".js-focus-visible :focus:not([data-focus-visible-added])": {
            outline: "none",
            boxShadow: "none",
        },
        // fixes the shifting of content on pages with / without scrollbars
        html: {
            overflowX: "hidden",
            marginRight: "calc(-1 * (100vw - 100%))",
        },
    }),
};

export const customTheme = {
    styles,
    colors: {
        ...ColorTheme,
    },
    components: {
        ...GeneralComponents,
        ...LayoutComponents,
    },
};

// Avoiding exporting all of ChakraUI base theme
export const { colors } = customTheme;

// Extend ChakraUI base theme
const theme = extendTheme({ ...customTheme });

/**
 * ChakraProvider for extending theme
 *
 * @returns ChakraProvider as a wrapper
 * @see {@link customTheme}
 */
const ChakraProviderContainer = ({ children }: any) => {
    return (
        <ChakraProvider theme={theme} resetCSS>
            {children}
        </ChakraProvider>
    );
};

export default ChakraProviderContainer;
