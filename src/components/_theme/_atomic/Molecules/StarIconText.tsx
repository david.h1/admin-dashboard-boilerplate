import { useColorModeValue, Text } from "@chakra-ui/react";
import StarIcon from "../Atoms/StarIcon";

const StarIconText_Molecule = ({ children }: any) => {
    return (
        <Text variant="Card_StarIconText">
            <StarIcon />
            {children}
        </Text>
    );
};

export default StarIconText_Molecule;
