import React, { ReactElement } from "react";

import Homepage_Flex, { homepageFlex } from "./Homepage_Flex";

interface Props {
    children?: React.ReactNode;
}

export default function Templates({ children }: Props): ReactElement {
    return <div>{children}</div>;
}

export { Homepage_Flex, homepageFlex };

/* 

* Template Types:
1/2 + 1/2
1/3 + 2/3
Grid

"Box_Stack" - pageStack




What could be responsive options?


"Flex", "Stack" goes in this one (Containers)


*/
