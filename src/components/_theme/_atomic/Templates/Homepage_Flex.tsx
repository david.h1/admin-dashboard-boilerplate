import { chakra, Flex, useStyleConfig, Box } from "@chakra-ui/react";

interface Props {
    /**
     * {@inheritDoc Book.writeFile}
     * @typeParam T
     */
    variant?: undefined | keyof typeof Variants;
    size?: string;
    children?: React.ReactNode;
}

export const Variants = {
    homepageContainer: ({ colorMode }: any) => ({
        flexDirection: {
            base: "column",
            xl: "row",
            lg: "row",
            md: "row",
            sm: "column",
        },
        width: { base: "100%", xl: "1200px", lg: "960px" },
        padding: { base: "0px", md: "0px 15px 0px 15px" },
        margin: "auto",
        marginTop: "30px",
        fontFamily: "Source Sans Pro",
    }),
    cardContainer: ({ colorMode }: any) => ({
        flexDirection: {
            base: "column",
        },
        width: { base: "auto", md: "100%" },
        margin: { base: 5, md: "auto" },
        alignItems: { base: "center", md: "flex-end" },
        flex: "3",
    }),
};

export const homepageFlex = {
    baseStyle: ({ colorMode }: any) => ({
        fontFamily: "Source Sans Pro",
    }),
    variants: {
        ...Variants,
    },
    sizes: {},
    defaultProps: {
        size: "",
        variant: "",
    },
};

/**
 *
 * @remarks
 * @see {@link CustomVariants}
 * @remarks Variants
 * @returns Custom Variant of Card_Box
 *
 * @beta
 */
export default function Homepage_Flex(props: Props) {
    const { variant, size, ...rest } = props;
    const styles = useStyleConfig("Homepage_Flex", { size, variant });
    return <Flex sx={styles} {...rest} />;
}
