import StarIconSvg from "../../../../assets/StarIcon.svg";

const StarIcon = () => {
    return (
        <img
            src={StarIconSvg}
            alt="Star Dot Point"
            style={{
                padding: "5px 15px 0px 0px",
                alignSelf: "flex-start",
            }}
        />
    );
};

export default StarIcon;
