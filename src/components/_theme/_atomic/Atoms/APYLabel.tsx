import { Box, useColorModeValue, Text } from "@chakra-ui/react";

const APYLabel = ({ APYContent }: any) => {
    return (
        <Box
            bg={useColorModeValue(
                "lightTheme.custom.label.bg",
                "darkTheme.custom.label.bg"
            )}
            color={useColorModeValue(
                "lightTheme.custom.label.fg",
                "darkTheme.custom.label.fg"
            )}
            px={"8px"}
            py={"6px"}
            borderRadius="5px"
            justifySelf="center"
            alignSelf="center"
        >
            <Text fontWeight="bold" fontSize="14px" whiteSpace="nowrap">
                {APYContent || "...Loading"}
            </Text>
        </Box>
    );
};

export default APYLabel;
