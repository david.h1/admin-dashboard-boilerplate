/* 
# Atomic Design Guide:

Atomic design is based upon:
- Atoms
- Molecules
- Organisms
- Templates
- Pages

The Goal of atomic design is reusability and taking ReactJS to its core, breaking components down into their constituent pieces.


The benefits of this system is making the code with content as readable as possible through separation of concerns. 


How does this work?

./theme/ -> contains entire custom theme file, this would be used by importing it into a new project and using these elements to create new project specific components and then readding them to the compnoent library for future use in projects. An elvoing dynamic system. 


Update navigation links:
1. ./pages/CONSTANTS.ts







IDEAS:

Create a custom prop for things that allows things to go 'lighter' or 'darker' on hover/active?

potential -stepUp1 // -stepDown1



Theme Switcher Tool (Top left corner on nav-bar) - that modifies a state call based upon what theme gets sent to chakraUI -> this includes the variant components




*/


│  │  └─ _theme
│  │     ├─ ColorTheme
│  │     │  ├─ ColorTheme.tsx
│  │     │  └─ ColorThemeGenerator.tsx
│  │     ├─ GeneralComponents
│  │     │  ├─ Button.tsx
│  │     │  └─ GeneralComponents.tsx
│  │     ├─ index.tsx
│  │     ├─ LayoutComponents
│  │     │  ├─ BackgroundBox.tsx
│  │     │  ├─ BadgeTest.tsx
│  │     │  ├─ BoxStack.tsx
│  │     │  ├─ CardBox.tsx
│  │     │  ├─ HeaderBox.tsx
│  │     │  └─ index.tsx
│  │     └─ _atomic
│  │        ├─ index.md
│  │        ├─ Molecules
│  │        │  └─ ButtonStack.tsx
│  │        └─ Templates
│  │           └─ index.tsx


./src/components/
/Theme/ -> Custom Atoms, Molecules, Templates
. -> Organisms (These will be the most used components so keeping them in the base directory of the components folder to increase visability)
./src/pages/
CONSTANTS
Pages
Page Routing