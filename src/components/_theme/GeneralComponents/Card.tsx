export const Card_Text = {
    Card_SubHeading: {
        py: { base: 4 },
        fontWeight: "medium",
    },
    Card_StarIconText: ({ colorMode }: any) => ({
        fontSize: "14px",
        my: { base: 2 },
        marginLeft: { base: 5 },
        display: "inline-flex",
        color: colorMode === "light" ? "lightTheme.dark.800" : "darkTheme.light.800",
    }),
};
