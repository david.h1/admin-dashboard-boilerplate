export const ButtonBaseStyle = {
    borderRadius: "15px",
    boxShadow: "2px 3px 15px 1px rgba(0,0,0,0.10)",
    color: "#FFF",
};

export const ButtonThemePrimary = {
    ...ButtonBaseStyle,
    bg: "lightTheme.primary.400",
    _hover: {
        bg: "lightTheme.primary.500",
        boxShadow: "0px 0px 5px 5px rgb(0 0 0 / 10%)",
        _disabled: {
            bg: "lightTheme.primary.500",
        },
    },
};

export const ButtonThemeSecondary = {
    ...ButtonBaseStyle,
    bg: "lightTheme.secondary.expanded",
    _hover: {
        bg: "lightTheme.secondary.600",
        boxShadow: "0px 0px 5px 5px rgb(0 0 0 / 10%)",
        _disabled: {
            bg: "lightTheme.secondary.600",
        },
    },
};

export const Button = {
    LAEFI: {
        ...ButtonThemePrimary,
    },
    LTOKEN: {
        ...ButtonThemeSecondary,
    },
    connectwallet: {
        display: { base: "none", md: "inline-flex" },
        fontSize: "sm",
        fontWeight: 700,
        color: "#FFF",
        bg: "lightTheme.primary.main",
        _hover: {
            bgGradient: "linear(to-br, lightTheme.primary.400, lightTheme.primary.600)",
        },
        _active: {
            bg: "lightTheme.primary.200",
        },
    },
};
