import { Button } from "./Button";
import { Card_Text } from "./Card";

const GeneralComponents = {
    Button: {
        variants: {
            ...Button,
        },
    },
    Text: {
        variants: {
            LAEFI: {
                color: "lightTheme.primary.400",
                display: "inline",
            },
            LTOKEN: {
                color: "lightTheme.secondary.expanded",
                display: "inline",
            },
            ModalInputHeader: {
                color: "gray.400",
                fontSize: "14px",
            },
            ModalPoolShare_Value: {
                fontWeight: "bold",
                fontSize: "14px",
            },
            ModalPoolShare_Type: {
                fontSize: "12px",
                textAlign: "center",
            },
            ...Card_Text,
        },
    },
    Input: {
        variants: {
            Modal_Deposit: {
                size: "lg",
                fontSize: "3xl",
                fontWeight: "bold",
                color: "gray.500",
                border: "0",
                p: "0",
                height: "50px",
                lineHeight: "30px",
            },
        },
    },
};

export default GeneralComponents;
