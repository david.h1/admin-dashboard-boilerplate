import React, { ReactElement } from "react";
import {
    Box,
    Container,
    Center,
    Flex,
    Spacer,
    Text,
    Square,
    Heading,
    Divider,
    useColorModeValue,
    Button,
} from "@chakra-ui/react";
import { toColorString, complement, readableColor } from "polished";

import { colors } from "../index";

interface Props {
    colorCode?: string;
    colorType?: string;
}

function getPropertyByString(object: any, propString: any) {
    let value = object;

    const props = propString.split(".");
    for (let index = 0; index < props.length; index += 1) {
        if (props[index] === undefined) break;
        value = value[props[index]];
    }
    return value;
}

export default function ColorThemeTest({}: Props): ReactElement {
    return (
        <>
            <ColorContainer colorType={"lightTheme.primary"}></ColorContainer>
            <ColorContainer colorType={"lightTheme.secondary"}></ColorContainer>
            <ColorContainer colorType={"lightTheme.dark"}></ColorContainer>
            <ColorContainer colorType={"lightTheme.light"}></ColorContainer>
        </>
    );
}

export function ColorContainer({ colorType }: Props): ReactElement {
    return (
        <Center>
            <ColorBox colorCode={`${colorType}.0`} />
            <ColorBox colorCode={`${colorType}.50`} />
            <ColorBox colorCode={`${colorType}.100`} />
            <ColorBox colorCode={`${colorType}.200`} />
            <ColorBox colorCode={`${colorType}.300`} />
            <ColorBox colorCode={`${colorType}.400`} />
            <ColorBox colorCode={`${colorType}.500`} />
            <ColorBox colorCode={`${colorType}.600`} />
            <ColorBox colorCode={`${colorType}.700`} />
            <ColorBox colorCode={`${colorType}.800`} />
            <ColorBox colorCode={`${colorType}.900`} />
        </Center>
    );
}

export function ColorBox({ colorCode }: Props): ReactElement {
    const completementColor = readableColor(getPropertyByString(colors, colorCode));
    return (
        <Flex
            w="150px"
            h="150px"
            bg={colorCode}
            color={completementColor}
            align="center"
            justify="center"
            textAlign="center"
            fontSize="12px"
        >
            {getPropertyByString(colors, colorCode).toUpperCase()}
            <br />
            {colorCode}
        </Flex>
    );
}
