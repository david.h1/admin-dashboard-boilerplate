import { darken, lighten, saturate, modularScale } from "polished";

// todo: colourScaleModifer Function
// Possible saturation/hue/and other PolishedJS Features
// const colorScaleModifer = [0.025, 0.1];
const colourScaleDarken = [0.025, 0.05, 0.075, 0.1, 0.125];
const colourScaleLighten = [0.1, 0.15, 0.2, 0.25, 0.3];

export const brandColours = {
    brand: {
        primary: "#F4C503",
        secondary: "#5979B9",
        dark: "#4F5C73",
        light: "#F1F1F1",
    },
    expandedBrand: {
        primary: "#D6A707",
        secondary: "#4B79C6",
        dark: "#3E4A5E",
        light: "#1C273F",
    },
};

const ColorTheme = {
    lightTheme: {
        primary: {
            main: brandColours.brand.primary,
            900: darken(colourScaleDarken[4], brandColours.brand.primary),
            800: darken(colourScaleDarken[3], brandColours.brand.primary),
            700: darken(colourScaleDarken[2], brandColours.brand.primary),
            600: darken(colourScaleDarken[1], brandColours.brand.primary),
            500: darken(colourScaleDarken[0], brandColours.brand.primary),
            400: brandColours.brand.primary,
            300: lighten(colourScaleLighten[0], brandColours.brand.primary),
            200: lighten(colourScaleLighten[1], brandColours.brand.primary),
            100: lighten(colourScaleLighten[2], brandColours.brand.primary),
            50: lighten(colourScaleLighten[3], brandColours.brand.primary),
            0: lighten(colourScaleLighten[4], brandColours.brand.primary),
            expanded: brandColours.expandedBrand.primary,
        },
        secondary: {
            900: darken(colourScaleDarken[4], brandColours.brand.secondary),
            800: darken(colourScaleDarken[3], brandColours.brand.secondary),
            700: darken(colourScaleDarken[2], brandColours.brand.secondary),
            600: darken(colourScaleDarken[1], brandColours.brand.secondary),
            500: darken(colourScaleDarken[0], brandColours.brand.secondary),
            400: brandColours.brand.secondary,
            300: lighten(colourScaleLighten[0], brandColours.brand.secondary),
            200: lighten(colourScaleLighten[1], brandColours.brand.secondary),
            100: lighten(colourScaleLighten[2], brandColours.brand.secondary),
            50: lighten(colourScaleLighten[3], brandColours.brand.secondary),
            0: lighten(colourScaleLighten[4], brandColours.brand.secondary),
            main: brandColours.brand.secondary,
            expanded: brandColours.expandedBrand.secondary,
        },
        dark: {
            main: brandColours.brand.dark,
            900: darken(colourScaleDarken[4], brandColours.brand.dark),
            800: darken(colourScaleDarken[3], brandColours.brand.dark),
            700: darken(colourScaleDarken[2], brandColours.brand.dark),
            600: darken(colourScaleDarken[1], brandColours.brand.dark),
            500: darken(colourScaleDarken[0], brandColours.brand.dark),
            400: brandColours.brand.dark,
            300: lighten(colourScaleLighten[0], brandColours.brand.dark),
            200: lighten(colourScaleLighten[1], brandColours.brand.dark),
            100: lighten(colourScaleLighten[2], brandColours.brand.dark),
            50: lighten(colourScaleLighten[3], brandColours.brand.dark),
            0: lighten(colourScaleLighten[4], brandColours.brand.dark),
        },
        light: {
            main: brandColours.brand.light,
            900: darken(colourScaleDarken[4], brandColours.brand.light),
            800: darken(colourScaleDarken[3], brandColours.brand.light),
            700: darken(colourScaleDarken[2], brandColours.brand.light),
            600: darken(colourScaleDarken[1], brandColours.brand.light),
            500: darken(colourScaleDarken[0], brandColours.brand.light),
            400: brandColours.brand.light,
            300: lighten(colourScaleLighten[0], brandColours.brand.light),
            200: lighten(colourScaleLighten[1], brandColours.brand.light),
            100: lighten(colourScaleLighten[2], brandColours.brand.light),
            50: lighten(colourScaleLighten[3], brandColours.brand.light),
            0: lighten(colourScaleLighten[4], brandColours.brand.light),
        },
        custom: {
            label: {
                bg: "#AEFFC9",
                fg: "#25B865",
            },
        },
        background: {
            header: {
                main: "#f1f1f1",
                foreground: "#f5f5f5",
            },
            card: {
                main: "#FCFCFC",
                shadow: "3px 3px 15px 1px #0000001f",
            },
        },
    },
    darkTheme: {
        primary: {
            main: brandColours.brand.primary,
            900: darken(colourScaleDarken[4], brandColours.brand.primary),
            800: darken(colourScaleDarken[3], brandColours.brand.primary),
            700: darken(colourScaleDarken[2], brandColours.brand.primary),
            600: darken(colourScaleDarken[1], brandColours.brand.primary),
            500: darken(colourScaleDarken[0], brandColours.brand.primary),
            400: brandColours.brand.primary,
            300: lighten(colourScaleLighten[0], brandColours.brand.primary),
            200: lighten(colourScaleLighten[1], brandColours.brand.primary),
            100: lighten(colourScaleLighten[2], brandColours.brand.primary),
            50: lighten(colourScaleLighten[3], brandColours.brand.primary),
            0: lighten(colourScaleLighten[4], brandColours.brand.primary),
            expanded: brandColours.expandedBrand.primary,
        },
        secondary: {
            900: darken(colourScaleDarken[4], brandColours.brand.secondary),
            800: darken(colourScaleDarken[3], brandColours.brand.secondary),
            700: darken(colourScaleDarken[2], brandColours.brand.secondary),
            600: darken(colourScaleDarken[1], brandColours.brand.secondary),
            500: darken(colourScaleDarken[0], brandColours.brand.secondary),
            400: brandColours.brand.secondary,
            300: lighten(colourScaleLighten[0], brandColours.brand.secondary),
            200: lighten(colourScaleLighten[1], brandColours.brand.secondary),
            100: lighten(colourScaleLighten[2], brandColours.brand.secondary),
            50: lighten(colourScaleLighten[3], brandColours.brand.secondary),
            0: lighten(colourScaleLighten[4], brandColours.brand.secondary),
            main: brandColours.brand.secondary,
            expanded: brandColours.expandedBrand.secondary,
        },
        dark: {
            main: brandColours.brand.dark,
            900: darken(colourScaleDarken[4], brandColours.brand.dark),
            800: darken(colourScaleDarken[3], brandColours.brand.dark),
            700: darken(colourScaleDarken[2], brandColours.brand.dark),
            600: darken(colourScaleDarken[1], brandColours.brand.dark),
            500: darken(colourScaleDarken[0], brandColours.brand.dark),
            400: brandColours.brand.dark,
            300: lighten(colourScaleLighten[0], brandColours.brand.dark),
            200: lighten(colourScaleLighten[1], brandColours.brand.dark),
            100: lighten(colourScaleLighten[2], brandColours.brand.dark),
            50: lighten(colourScaleLighten[3], brandColours.brand.dark),
            0: lighten(colourScaleLighten[4], brandColours.brand.dark),
        },
        light: {
            main: brandColours.brand.light,
            900: darken(colourScaleDarken[4], brandColours.brand.light),
            800: darken(colourScaleDarken[3], brandColours.brand.light),
            700: darken(colourScaleDarken[2], brandColours.brand.light),
            600: darken(colourScaleDarken[1], brandColours.brand.light),
            500: darken(colourScaleDarken[0], brandColours.brand.light),
            400: brandColours.brand.light,
            300: lighten(colourScaleLighten[0], brandColours.brand.light),
            200: lighten(colourScaleLighten[1], brandColours.brand.light),
            100: lighten(colourScaleLighten[2], brandColours.brand.light),
            50: lighten(colourScaleLighten[3], brandColours.brand.light),
            0: lighten(colourScaleLighten[4], brandColours.brand.light),
        },
        custom: {
            label: {
                bg: "transparent",
                // bg: "#045B2A",
                fg: "#00FF77",
            },
        },
        background: {
            header: {
                main: brandColours.brand.dark,
                foreground: brandColours.brand.dark,
            },
            card: {
                main: "#48556B",
                shadow: "3px 3px 15px 1px #0000001f",
            },
        },
    },
    background: {
        darkest: "#001028ab",
        lightest: "#ffffffb0",
    },
};

export default ColorTheme;
