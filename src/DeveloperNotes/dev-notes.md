## ICS Admin Dashboard Project:

### Functionality:

-   Add/Remove/Block/Unblock Businesses (Blockchain Aspect?)
-   Authentication (Auth0v2)

## First Pull Request:

Packages Installed:

-   ChakraUI
-   EmotionCss
-   dotenv
-   ethers
-   momentjs
-   polished
-   react-spring
-   @testing-lbirary/react-hooks
-   enzyme + enzyme adapter
-   eslint
-   babel-loader
    -prettier
-   eslint
-   react-query
-   react-router-dom
