export interface NavItem {
    label: string;
    subLabel?: string;
    children?: Array<NavItem>;
    href?: string;
    to?: any;
}

export const NAV_ITEMS: Array<NavItem> = [
    {
        label: "Dashboard",
        href: "/",
    },
    {
        label: "Business",
        href: "/business",
    },
    {
        label: "Users",
        href: "/users",
    },
    {
        label: "Login Tester",
        href: "/login",
    },
    {
        label: "Sub-page Navigation #1",
        href: "#",
        children: [
            {
                label: `Sub-Nav #1`,
                subLabel: "Sub-nav ",
                href: `/subnav/1`,
            },
            {
                label: `Sub-Nav #2`,
                subLabel: "Provide a Uniswap-V3 NFT and receive rewards",
                href: `/subnav/2`,
            },
        ],
    },
];
