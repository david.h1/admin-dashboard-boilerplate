import {
    Box,
    Flex,
    Text,
    IconButton,
    Button,
    Stack,
    Collapse,
    Icon,
    Link,
    Popover,
    PopoverTrigger,
    PopoverContent,
    useColorModeValue,
    useBreakpointValue,
    useDisclosure,
    useColorMode,
} from "@chakra-ui/react";
import React, { ReactElement, useContext } from "react";
import {
    HamburgerIcon,
    CloseIcon,
    ChevronDownIcon,
    ChevronRightIcon,
    MoonIcon,
    SunIcon,
} from "@chakra-ui/icons";
import { Link as RouteLink } from "react-router-dom";
import { NAV_ITEMS, NavItem } from "../CONSTANTS";
import { AuthenticationContext } from "../../_globals/context/AuthenticationContext";

export default function SideNavigation(): ReactElement {
    const { isOpen, onToggle } = useDisclosure();
    const { toggleColorMode } = useColorMode();
    const { isAuthenticated, Handle_SetIsAuthenticated } =
        useContext(AuthenticationContext);
    return (
        <Box
            // bg={useColorModeValue("lightTheme.dark.main", "lightTheme.dark.main")}
            bg="white"
            // style={{ zIndex: 10, position: "relative" }}
            // boxShadow="0px -1px 10px 12px rgb(0 0 0 / 15%)"
            height="100vh"
            width="150px"
        >
            <Flex
                color={useColorModeValue("black", "black")}
                // minH={"60px"}
                py={{ base: 2 }}
                px={{ base: 4 }}
                borderBottom={1}
                borderStyle={"solid"}
                borderColor={useColorModeValue("transparent", "transparent")}
                align={"center"}
                maxWidth="150px"
                height="100vh"
                direction="column"
                // width={{ base: "100%", xl: "1200px", lg: "960px" }}
                // margin={"auto"}
            >
                <Flex
                    flex={{ base: 1, md: "auto" }}
                    ml={{ base: -2 }}
                    display={{ base: "flex", md: "none" }}
                >
                    <IconButton
                        onClick={onToggle}
                        icon={
                            isOpen ? (
                                <CloseIcon w={3} h={3} />
                            ) : (
                                <HamburgerIcon w={5} h={5} />
                            )
                        }
                        variant={"ghost"}
                        aria-label={"Toggle Navigation"}
                    />
                </Flex>
                <Flex
                    flex={{ base: 1 }}
                    justify={{ base: "center", md: "start" }}
                    direction="column"
                >
                    <Text
                        fontFamily={"heading"}
                        textAlign={"center"}
                        color={useColorModeValue("gray.800", "black")}
                    >
                        <Link
                            as={RouteLink}
                            p={2}
                            to="/"
                            fontSize={"sm"}
                            fontWeight={500}
                        >
                            ICS LOGO
                        </Link>
                    </Text>
                    <Flex display={{ base: "none", md: "flex" }}>
                        <DesktopNav />
                        <Text onClick={() => Handle_SetIsAuthenticated("logout")}>
                            LOGIN CLICKER
                        </Text>
                    </Flex>
                </Flex>
                <Stack
                    flex={{ base: 1, md: 0 }}
                    justify={"flex-end"}
                    direction={"column"}
                    spacing={6}
                >
                    <Button
                        as={"a"}
                        fontSize={"sm"}
                        fontWeight={400}
                        variant={"ghost"}
                        href={"#"}
                        onClick={toggleColorMode}
                        _hover={{
                            bg: "whiteAlpha.200",
                        }}
                        _active={{
                            bg: "whiteAlpha.300",
                        }}
                        display={{
                            base: "none",
                            sm: "inherit",
                        }}
                        visibility={{
                            base: "hidden",
                            md: "visible",
                        }}
                    >
                        {useColorModeValue(
                            <MoonIcon w={7} h={7} color="white" />,
                            <SunIcon w={7} h={7} color="white" />
                        )}
                    </Button>
                </Stack>
            </Flex>
            <Collapse in={isOpen} animateOpacity>
                <MobileNav />
            </Collapse>
        </Box>
    );
}

const DesktopNav = () => {
    const linkColor = useColorModeValue("black", "black");
    const linkHoverColor = useColorModeValue("gray.200", "gray.200");
    const popoverContentBgColor = useColorModeValue(
        "lightTheme.dark.main",
        "darkTheme.dark.main"
    );

    return (
        <Stack direction={"column"} spacing={4} alignItems={"center"}>
            {NAV_ITEMS.map((navItem) => (
                <Box key={navItem.label}>
                    <Popover trigger={"hover"} placement={"bottom-start"}>
                        <PopoverTrigger>
                            <Link
                                as={RouteLink}
                                p={2}
                                href={navItem.href ?? "#"}
                                to={navItem.href! ?? "#"}
                                fontSize={"sm"}
                                fontWeight={500}
                                color={linkColor}
                                _hover={{
                                    textDecoration: "none",
                                    color: linkHoverColor,
                                }}
                            >
                                {navItem.label}
                            </Link>
                        </PopoverTrigger>

                        {navItem.children && (
                            <PopoverContent
                                border={0}
                                boxShadow={"xl"}
                                bg={popoverContentBgColor}
                                p={4}
                                rounded={"xl"}
                                minW={"sm"}
                            >
                                <Stack>
                                    {navItem.children.map((child) => (
                                        <DesktopSubNav key={child.label} {...child} />
                                    ))}
                                </Stack>
                            </PopoverContent>
                        )}
                    </Popover>
                </Box>
            ))}
        </Stack>
    );
};

const DesktopSubNav = ({ label, href, subLabel }: NavItem) => {
    return (
        <Link
            as={RouteLink}
            to={href!}
            role={"group"}
            display={"block"}
            p={2}
            rounded={"md"}
            _hover={{ bg: useColorModeValue("gray.600", "darkTheme.dark.main") }}
        >
            <Stack direction={"row"} align={"center"}>
                <Box>
                    <Text
                        transition={"all .3s ease"}
                        _groupHover={{ color: "yellow.400" }}
                        fontWeight={500}
                    >
                        {label}
                    </Text>
                    <Text fontSize={"sm"}>{subLabel}</Text>
                </Box>
                <Flex
                    transition={"all .3s ease"}
                    transform={"translateX(-10px)"}
                    opacity={0}
                    _groupHover={{
                        opacity: "100%",
                        transform: "translateX(0)",
                    }}
                    justify={"flex-end"}
                    align={"center"}
                    flex={1}
                >
                    <Icon color={"yellow.400"} w={5} h={5} as={ChevronRightIcon} />
                </Flex>
            </Stack>
        </Link>
    );
};

const MobileNav = () => {
    return (
        <Stack bg={useColorModeValue("white", "gray.800")} p={4} display={{ md: "none" }}>
            {NAV_ITEMS.map((navItem) => (
                <MobileNavItem key={navItem.label} {...navItem} />
            ))}
        </Stack>
    );
};

const MobileNavItem = ({ label, children, href }: NavItem) => {
    const { isOpen, onToggle } = useDisclosure();

    return (
        <Stack spacing={4} onClick={children && onToggle}>
            <Flex
                py={2}
                as={Link}
                href={href ?? "#"}
                justify={"space-between"}
                align={"center"}
                _hover={{
                    textDecoration: "none",
                }}
            >
                <Text fontWeight={600} color={useColorModeValue("gray.600", "gray.200")}>
                    {label}
                </Text>
                {children && (
                    <Icon
                        as={ChevronDownIcon}
                        transition={"all .25s ease-in-out"}
                        transform={isOpen ? "rotate(180deg)" : ""}
                        w={6}
                        h={6}
                    />
                )}
            </Flex>

            <Collapse in={isOpen} animateOpacity style={{ marginTop: "0 !important" }}>
                <Stack
                    mt={2}
                    pl={4}
                    borderLeft={1}
                    borderStyle={"solid"}
                    borderColor={useColorModeValue("gray.200", "gray.700")}
                    align={"start"}
                >
                    {children &&
                        children.map((child) => (
                            <Link
                                as={RouteLink}
                                key={child.label}
                                py={2}
                                // href={child.href}
                                to={child.href!}
                            >
                                {child.label}
                            </Link>
                        ))}
                </Stack>
            </Collapse>
        </Stack>
    );
};
