import React, { ReactElement } from "react";
import {
    Box,
    Flex,
    Text,
    IconButton,
    Button,
    Stack,
    Collapse,
    Icon,
    Link,
    Popover,
    PopoverTrigger,
    PopoverContent,
    useColorModeValue,
    useBreakpointValue,
    useDisclosure,
    useColorMode,
    Container,
    Heading,
} from "@chakra-ui/react";

export default function Business(): ReactElement {
    return (
        <>
            <Box>
                <Flex
                    bg="gray.50"
                    maxWidth="100%"
                    minHeight="150px"
                    textAlign="center"
                    align="center"
                    p={5}
                    borderRadius="5px"
                    border="1px"
                    borderColor="gray.300"
                >
                    <Heading size="md">Business header</Heading>
                </Flex>
            </Box>
            <Box>Business</Box>
        </>
    );
}
