import React, { createContext } from "react";
import { mount, shallow } from "enzyme";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router";
import App, { AuthenticationConsumer } from "../../App";
import PageNotFound404 from "../PageNotFound404";
import Dashboard from "../Dashboard";
import SubNavPage from "../SubNavPage";
import TopNavigation from "../Navigation/TopNavigation";
import Login from "../Login";
import { ApplicationContainer } from "../../App";
import AuthenticationProvider, { AuthenticationContext } from "../../_globals/context/AuthenticationContext";
it("Mounts router/navigation without crashing", () => {
    const isAuthenticated = true;
    const wrapper = mount(
        <MemoryRouter>
            <ApplicationContainer isAuthenticated={isAuthenticated} />
        </MemoryRouter>
    );
    expect(wrapper.find(PageNotFound404)).toHaveLength(0);
    expect(wrapper.find(Dashboard)).toHaveLength(1);
});

it("mounts application and tests 404 page", () => {
    const isAuthenticated = true;
    const wrapper = mount(
        <MemoryRouter initialEntries={["/random"]}>
            <ApplicationContainer isAuthenticated={isAuthenticated} />
        </MemoryRouter>
    );
    expect(wrapper.find(SubNavPage)).toHaveLength(0);
    expect(wrapper.find(TopNavigation)).toHaveLength(1);
    expect(wrapper.find(PageNotFound404)).toHaveLength(1);
});

it("Not Authenticated and going to route /login", () => {
    const isAuthenticated = false;
    const wrapper = mount(
        <MemoryRouter initialEntries={["/login"]}>
            <ApplicationContainer isAuthenticated={isAuthenticated} />
        </MemoryRouter>
    );
    expect(wrapper.find(Dashboard)).toHaveLength(0);
    expect(wrapper.find(Login)).toHaveLength(1);
});

it("Not Authenticated and going to a 404 route takes you back to /login", () => {
    const isAuthenticated = false;
    const wrapper = mount(
        <MemoryRouter initialEntries={["/NotTheLoginURL"]}>
            <ApplicationContainer isAuthenticated={isAuthenticated} />
        </MemoryRouter>
    );
    expect(wrapper.find(Dashboard)).toHaveLength(0);
    expect(wrapper.find(Login)).toHaveLength(1);
});
it("renders <AuthenticationConsumer /> without crashing", () => {
    const mockCallback = jest.fn();
    const wrapper = mount(
        <MemoryRouter>
            <AuthenticationConsumer />
        </MemoryRouter>,
        {
            wrappingComponent: AuthenticationContext.Provider,
            wrappingComponentProps: {
                value: { isAuthenticated: false, Handle_SetIsAuthenticated: mockCallback },
            },
        }
    );
    wrapper.find("Button").simulate("click");
    expect(mockCallback).toHaveBeenCalledTimes(1);
});
