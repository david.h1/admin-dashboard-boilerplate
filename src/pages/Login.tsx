import React, { ReactElement, useContext } from "react";
import {
    Box,
    Flex,
    Text,
    IconButton,
    Button,
    Stack,
    Collapse,
    Icon,
    Link,
    Popover,
    PopoverTrigger,
    PopoverContent,
    useColorModeValue,
    useBreakpointValue,
    useDisclosure,
    useColorMode,
    Container,
    Heading,
} from "@chakra-ui/react";
import { AuthenticationContext } from "../_globals/context/AuthenticationContext";
export default function Login(): ReactElement {
    const { Handle_SetIsAuthenticated } = useContext(AuthenticationContext);
    return (
        <>
            <Box>THIS IS THE LOGIN COMPONENT</Box>
            <Button onClick={() => Handle_SetIsAuthenticated("login")}>LOGIN CLICKER</Button>
        </>
    );
}
