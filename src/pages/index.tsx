import React, { useEffect, useState, useContext, ReactElement } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "./Dashboard";
import Business from "./Business";
import Users from "./Users";
import Login from "./Login";
import SubNavPage from "./SubNavPage";
import PageNotFound404 from "./PageNotFound404";
import { Box, Stack, useColorModeValue } from "@chakra-ui/react";
import SideNavigation from "./Navigation/SideNavigation";
import TopNavigation from "./Navigation/TopNavigation";

export default function PageSwitcher({ isAuthenticated }: any): ReactElement {
    return (
        <Switch>
            <UnProtectedRoute exact path={`/login`} isAuthenticated={isAuthenticated} component={Login} />
            <ProtectedRoute exact path="/" isAuthenticated={isAuthenticated} component={Dashboard} />
            <ProtectedRoute exact path="/users" isAuthenticated={isAuthenticated} component={Users} />
            <ProtectedRoute exact path="/business" isAuthenticated={isAuthenticated} component={Business} />

            <Route path={`/subnav/1`} exact>
                <SubNavPage />
            </Route>
            <Route path={`/subnav/2`} exact>
                <SubNavPage />
            </Route>
            {isAuthenticated ? (
                <ProtectedRoute isAuthenticated={isAuthenticated} component={PageNotFound404} />
            ) : (
                <>
                    <Route component={PageNotFound404} />
                    <Redirect to="/login" />
                </>
            )}
        </Switch>
    );
}

const ProtectedRoute = ({ component: Component, isAuthenticated, ...restOfProps }: any) => {
    const bgColor = useColorModeValue("gray.50", "gray.200");
    return (
        <Route
            {...restOfProps}
            render={(props) =>
                isAuthenticated ? (
                    <Stack direction="row">
                        <SideNavigation />
                        <Box width="100%" height="100%" p={0} m={0} style={{ marginInlineStart: "0px" }}>
                            <TopNavigation />

                            <Box p={5} height={`calc(100vh - 60px)`} bg={bgColor}>
                                <Component {...props} />
                            </Box>
                        </Box>
                    </Stack>
                ) : (
                    <Redirect to="/login" />
                )
            }
        />
    );
};

const UnProtectedRoute = ({ component: Component, isAuthenticated, ...restOfProps }: any) => {
    return (
        <Route
            {...restOfProps}
            render={(props) => (isAuthenticated === false ? <Component {...props} /> : <Redirect to="/" />)}
        />
    );
};
